import React, {useCallback, useState} from 'react';
import {Button, TextField, Form, FormLayout} from '@shopify/polaris';

export default function QuoteInput() {
  const [quote, setQuote] = useState('');

  const handleSubmit = useCallback((_event) => {
    setQuote('');
  }, []);

  const handleQuoteChange = useCallback((value) => setQuote(value), []);

  return (
    <Form onSubmit={handleSubmit}>
      <FormLayout>

        <TextField
          value={quote}
          onChange={handleQuoteChange}
          label="Your Quote"
          type="quote"
          placeholder="your quote"
        />

        <Button submit>Submit</Button>
      </FormLayout>
    </Form>
  );
}