import React, {useState} from 'react';
import {ColorPicker} from '@shopify/polaris';

export default function ColorPicker() {
  const [color, setColor] = useState({
    hue: 120,
    brightness: 1,
    saturation: 1,
  });

  return <ColorPicker onChange={setColor} color={color} />;
}