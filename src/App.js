import "./App.css";
import enTranslations from "@shopify/polaris/locales/en.json";
import { AppProvider, Page, Card, Button } from "@shopify/polaris";
import QuoteInput from "./components/QuoteInput";

function App() {
  return (
    <div className="App">
      <AppProvider i18n={enTranslations}>
        <Page>
          <QuoteInput />
        </Page>
      </AppProvider>
    </div>
  );
}
export default App;
